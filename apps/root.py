import io
import logging
import queue
import re
from multiprocessing import Process, Queue

from flask import request

from apps.globals import AREA_MAX
from apps.globals import Error
from apps.globals import HEIGHT_MIN, HEIGHT_MAX
from apps.globals import HTTP
from apps.globals import MAX_DAYS
from apps.globals import NFREQ_MIN, NFREQ_MAX
from apps.globals import TIMEOUT
from apps.globals import WIDTH_MIN, WIDTH_MAX
from apps.output import get_output
from apps.parameters import Parameters
from apps.utils import check_base_parameters
from apps.utils import check_request
from apps.utils import currentutcday
from apps.utils import error_param
from apps.utils import error_request
from apps.utils import is_valid_color
from apps.utils import is_valid_datetime
from apps.utils import is_valid_integer
from apps.utils import is_valid_nodata
from apps.utils import is_valid_output
from apps.utils import is_valid_units
from apps.utils import is_valid_spacing
from apps.utils import is_valid_starttime


def check_parameters(params):

    # check base parameters
    (params, error) = check_base_parameters(params, MAX_DAYS)
    if error["code"] != 200:
        return (params, error)

    # Plot parameter validations
    if not is_valid_color(params["color"]):
        return error_param(params, Error.COLOR + str(params["color"]))

    if not is_valid_integer(params["width"], WIDTH_MIN, WIDTH_MAX):
        return error_param(params, Error.WIDTH + str(params["width"]))
    params["width"] = int(params["width"])

    if not is_valid_integer(params["height"], HEIGHT_MIN, HEIGHT_MAX):
        return error_param(params, Error.HEIGHT + str(params["height"]))
    params["height"] = int(params["height"])

    if params["width"] * params["height"] > AREA_MAX:
        return error_param(params, Error.AREA)

    # nfreq parameter validation
    if not is_valid_integer(params["nfreq"], NFREQ_MIN, NFREQ_MAX):
        return error_param(params, Error.NFREQ + str(params["nfreq"]))
    params["nfreq"] = int(params["nfreq"])

    # min_freq < max_freq ?
    if params["maxfreq"] is not None:
        if params["minfreq"] >= params["maxfreq"]:
            return error_param(params, Error.MINMAXFREQ)

    # Units validation
    if not is_valid_units(params["units"]):
        return error_param(params, Error.UNITS + str(params["units"]))
    params["units"] = params["units"].upper()

    # Spacing validation
    if not is_valid_spacing(params["spacing"]):
        return error_param(params, Error.SPACING + str(params["spacing"]))
    params["spacing"] = params["spacing"].lower()

    # time validations
    if params["time"] is not None:
        if not is_valid_starttime(params["time"]):
            return error_param(params, Error.TIME + str(params["time"]))

        if is_valid_datetime(params["time"]):
            params["time"] = is_valid_datetime(params["time"])
    else:
        params["time"] = currentutcday()

    # output parameter validation
    if not is_valid_output(params["format"]):
        return error_param(params, Error.OUTPUT + str(params["format"]))
    params["format"] = params["format"].lower()

    # nodata parameter validation
    if not is_valid_nodata(params["nodata"]):
        return error_param(params, Error.NODATA_CODE + str(params["nodata"]))
    params["nodata"] = params["nodata"].lower()

    # wildcards or list are not allowed
    for key in ("network", "station", "location", "channel"):
        if re.search(r"[,*?]", params[key]):
            return error_param(params, Error.NO_WILDCARDS + key)

    #    for key, val in params.items():
    #        logging.debug(key + ": " + str(val))

    return (params, {"msg": HTTP._200_, "details": Error.VALID_PARAM, "code": 200})


def checks_get():

    # get default parameters
    params = Parameters().todict()

    # check if the parameters are unknown
    (p, result) = check_request(params)
    if result["code"] != 200:
        return (p, result)
    return check_parameters(params)


def checks_post(params):

    for key, val in Parameters().todict().items():
        if key not in params:
            params[key] = val
    return check_parameters(params)


def get_post_params():
    rows = list()
    params = dict()
    paramslist = list()
    code = ("network", "station", "location", "channel")
    exclude = ("net", "sta", "cha", "loc", "starttime", "endtime", "constraints")
    post_params = (k for k in Parameters().todict() if k not in exclude)

    # Universal newline decoding :
    stream = io.StringIO(request.stream.read().decode("UTF8"), newline=None)
    for row in stream:
        row = row.strip()  # Remove leading and trailing whitespace.
        row = re.sub(r"[\s]+", " ", row)
        if re.search(r"[^a-zA-Z0-9_,* =?.:-]", row) is None:  # Is a valid row ?
            if re.search(r"=", row):  # parameter=value pairs
                key, val = row.replace(" ", "").split("=")
                if key in post_params:
                    params[key] = val
            else:
                rows.append(row)
    for row in rows:
        row = row.split(" ")
        if len(row) >= 4:
            paramslist.append({code[i]: row[i] for i in range(0, 4)})
            paramslist[-1].update(params)
            if len(row) == 5:  # Per line time parameter
                paramslist[-1].update({"time": row[4]})
    return paramslist


def output():

    try:
        process = None
        valid_param_dicts = list()
        result = {"msg": HTTP._400_, "details": Error.UNKNOWN_PARAM, "code": 400}
        logging.debug(request.url)

        if request.method == "POST":
            for params in get_post_params():
                (params, result) = checks_post(params)
                if result["code"] == 200:
                    valid_param_dicts.append(params)
        else:
            (params, result) = checks_get()
            if result["code"] == 200:
                valid_param_dicts.append(params)

        if valid_param_dicts:

            def put_response(q):
                q.put(get_output(valid_param_dicts))

            q = Queue()
            process = Process(target=put_response, args=(q,))
            process.start()
            resp = q.get(timeout=TIMEOUT)

            if resp:
                return resp
            else:
                raise Exception

    except queue.Empty:
        result = {"msg": HTTP._408_, "details": Error.TIMEOUT, "code": 408}

    except Exception as excep:
        result = {"msg": HTTP._500_, "details": Error.UNSPECIFIED, "code": 500}
        logging.exception(str(excep))

    finally:
        if process:
            process.terminate()

    return error_request(
        msg=result["msg"], details=result["details"], code=result["code"]
    )
