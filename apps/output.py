import io
import logging
import time
from tempfile import SpooledTemporaryFile
from zipfile import ZipFile, ZIP_DEFLATED

import numpy as np
import matplotlib.pyplot as plt
from flask import make_response
from obspy.clients.fdsn import Client

from apps.globals import Error
from apps.globals import FDSN_CLIENT
from apps.globals import USER_AGENT_EVALRESP
from apps.utils import error_request
from apps.utils import error_500
from apps.utils import tictac


pi_ticks_labels = (
    r"$-\pi$",
    r"$-5\pi\slash{6}$",
    r"$-2\pi\slash{3}$",
    r"$-\pi\slash{2}$",
    r"$-\pi\slash{3}$",
    r"$-\pi\slash{6}$",
    "0",
    r"$\pi\slash{6}$",
    r"$\pi\slash{3}$",
    r"$\pi\slash{2}$",
    r"$2\pi\slash{3}$",
    r"$5\pi\slash{6}$",
    r"$\pi$",
)


def records_to_text(data, sep=" "):
    data = data.tolist()
    return "".join(f"{sep.join(str(format(x, '.6E')) for x in row)}\n" for row in data)


def bode_plot(params, freqs, cpx_response, inventory):
    img = None
    nyquist = params["nyquist"]
    sensitivity = inventory.response.instrument_sensitivity.value
    sens_freq = inventory.response.instrument_sensitivity.frequency

    size = 8.0 * max(params["width"], params["height"]) / 800
    plt_params = {
        "font.size": size,
        "figure.titlesize": size * 1.1,
        "figure.titleweight": "bold",
        "axes.labelsize": size * 1.1,
        "axes.labelpad": size,
        "xtick.labelsize": size * 0.85,
        "ytick.labelsize": size * 0.85,
    }
    plt.rcParams.update(plt_params)

    offset = 0.005
    pcolor = "#" + params["color"]
    style1 = dict(ha="left", xycoords="axes fraction")
    style2 = dict(ha="right", xycoords="axes fraction")
    fig = plt.figure(figsize=(params["width"] // 100, params["height"] // 100))

    if params["format"] in ("plot-amp", "plot"):
        # plot amplitude response
        if params["format"] == "plot-amp":
            ax1 = fig.add_subplot(111)
        else:
            ax1 = fig.add_subplot(211)

        ax1.loglog(freqs, abs(cpx_response), color=pcolor)
        text = f"Gain [{params['output_units']} / {params['input_units']}]"
        ax1.set_ylabel(text)

        if sensitivity and params["annotate"]:
            ax1.axhline(sensitivity, ls="--", color="green")
            data_to_axis = ax1.transData + ax1.transAxes.inverted()
            xaxis, yaxis = data_to_axis.transform((1, sensitivity))
            text = f"sens gain: {format(sensitivity, '.5E')}"
            ax1.annotate(text, xy=(0.01, yaxis - offset * 5), va="top", **style1)

    if params["format"] in ("plot-phase", "plot"):
        # plot phase response
        if params["format"] == "plot-phase":
            ax2 = fig.add_subplot(111)
        else:
            ax2 = fig.add_subplot(212)

        phase = np.angle(cpx_response, deg=params["degrees"])
        ax2.semilogx(freqs, phase, color=pcolor)

        if params["degrees"]:
            ax2.set_ylabel("Phase [degrees]")
            ax2.set_yticks(np.arange(-180, 181, 30))
            ax2.set_yticklabels(np.arange(-180, 181, 30))
            ax2.set_ylim(-200, 200)
        else:
            ax2.set_ylabel("Phase [rad]")
            ax2.set_yticks(np.radians(np.arange(-180, 181, 30)))
            ax2.set_yticklabels(pi_ticks_labels, rotation="horizontal")
            ax2.set_ylim(-3.49, 3.49)

    for ax in fig.axes:
        ax.grid(which="major", axis="both", linestyle="-")
        ax.grid(which="minor", axis="both", linestyle=":")
        ax.tick_params(axis="both", which="both")
        if params["annotate"]:
            data_to_axis = ax.transData + ax.transAxes.inverted()
            # plot nyquist frequency
            if params["minfreq"] <= nyquist <= params["maxfreq"]:
                ax.axvline(nyquist, ls="--", color="red")
                text = f"nyquist: {nyquist} Hz"
                xaxis, _ = data_to_axis.transform((nyquist, 1))
                nt = ax.annotate(text, xy=(xaxis - offset, 0.1), **style2)
                if nt.get_position()[0] < 0.15:
                    nt.set_ha("left")
            # plot sensitivity frequency
            if sens_freq and params["minfreq"] <= sens_freq <= params["maxfreq"]:
                ax.axvline(sens_freq, ls="--", color="red")
                text = f"sens freq: {sens_freq} Hz"
                xaxis, _ = data_to_axis.transform((sens_freq, 1))
                st = ax.annotate(text, xy=(xaxis - offset, 0.2), **style2)
                if st.get_position()[0] < 0.2:
                    st.set_ha("left")

    ax.set_xlabel("Frequency [Hz]")
    start = inventory.start_date.isoformat()
    end = inventory.end_date.isoformat()
    cid = f"{params['network']}.{params['station']}.{params['location']}.{params['channel']}"
    title = f"\n{start}   to   {end}"
    fig.suptitle(cid + title)
    fig.align_labels()
    plt.tight_layout(rect=[0.0, 0.0, 1.0, 0.94])
    logging.debug("Saving figure...")
    img = io.BytesIO()
    plt.savefig(img, format="png")
    plt.close()
    return img


def get_input_units_type(params, response):
    """Returns the unit type should be VEL, ACC or DISP."""

    params["input_units"] = response.instrument_sensitivity.input_units
    # logging.debug("input_units: " + params["input_units"])
    params["output_units"] = response.instrument_sensitivity.output_units
    # logging.debug("output_units: " + params["output_units"])
    description = response.instrument_sensitivity.input_units_description
    # logging.debug("units_description: " + description)

    units = "DISP" if params["units"] == "DIS" else params["units"]
    if units == "DEF":
        if params["input_units"] and params["input_units"].upper() == "M/S":
            units = "VEL"
        elif params["input_units"] and params["input_units"].upper() == "M/S**2":
            units = "ACC"
        else:
            units = "DISP"
    return units


def get_complex_response(params, response):
    """Returns the complex instrumental response."""

    sampling_rates = response.get_sampling_rates()
    last_stage = max(sampling_rates.keys())
    sampling_rate = sampling_rates[last_stage]["output_sampling_rate"]
    params["nyquist"] = sampling_rate / 2.0

    sens_freq = response.instrument_sensitivity.frequency
    if not params["maxfreq"]:
        params["maxfreq"] = sampling_rate
        if sens_freq and sens_freq > sampling_rate:
            params["maxfreq"] = sens_freq

    if params["spacing"] in ("lin", "linear"):
        freqs = np.linspace(
            params["minfreq"], params["maxfreq"], params["nfreq"], dtype=np.float64
        )
    else:
        freqs = np.geomspace(
            params["minfreq"], params["maxfreq"], params["nfreq"], dtype=np.float64
        )

    units = get_input_units_type(params, response)
    cpx_response = response.get_evalresp_response_for_frequencies(freqs, output=units)
    return freqs, cpx_response


def get_inventory(params):
    client = Client(FDSN_CLIENT, user_agent=USER_AGENT_EVALRESP)
    return client.get_stations(
        network=params["network"],
        station=params["station"],
        location=params["location"],
        channel=params["channel"],
        starttime=params["time"],
        endtime=params["time"],
        level="response",
    )


def nodata_error(params):
    code = int(params["nodata"])
    return error_request(msg=f"HTTP._{code}_", details=Error.NODATA, code=code)


def get_output(param_dic_list):
    """
    Builds the ws-evalresp response

    Parameters:
        param_dic_list: list of parameter dictionaries
    Returns:
        response: response with text, json or csv with data availability
    """

    try:
        tic = time.time()
        msg = ""
        response = None
        tmp = SpooledTemporaryFile(max_size=1000 ** 2)
        with ZipFile(tmp, "w", ZIP_DEFLATED) as archive:
            for params in param_dic_list:
                cid = f"{params['network']}.{params['station']}.{params['location']}.{params['channel']}"
                cid = cid + "." + str(params["time"]).replace(" ", "T")
                logging.debug(cid)

                try:
                    inventory = get_inventory(params)
                    if len(inventory[0][0]) > 1:
                        logging.warning("Many channels found. We keep the first one.")
                except Exception as ex:
                    inventory = None
                    logging.debug(str(ex))
                    archive.writestr(f"{cid}.csv", "NODATA")
                    continue

                try:
                    if len(inventory[0][0]) == 0:
                        msg = "No channel found!"
                        logging.warning(msg)
                        if len(param_dic_list) == 1:
                            return nodata_error(params)
                        raise Exception
                    inventory = inventory[0][0][0]  # first channel inventory
                    inv_response = inventory.response
                    if not inv_response:
                        msg = "Response is not defined!"
                        logging.warning(msg)
                        if len(param_dic_list) == 1:
                            return nodata_error(params)
                        raise Exception
                    if not inv_response.instrument_sensitivity:
                        msg = "Instrument sensitivity is not defined!"
                        logging.warning(msg)
                        if len(param_dic_list) == 1:
                            return nodata_error(params)
                        raise Exception

                    freqs, cpx_resp = get_complex_response(params, inv_response)
                    if params["format"] == "cs":
                        # cs — Three column ASCII (frequency, real, imaginary)
                        data = np.vstack((freqs, cpx_resp.real, cpx_resp.imag)).T
                    elif params["format"] == "fap":
                        # fap — Three column ASCII (frequency, amplitude, phase)
                        phase = np.angle(cpx_resp, deg=True)
                        data = np.vstack((freqs, abs(cpx_resp), phase)).T
                    else:
                        img = bode_plot(params, freqs, cpx_resp, inventory)
                        if img is None and len(param_dic_list) == 1:
                            return error_500(Error.UNSPECIFIED)

                    if params["format"] in ("fap", "cs"):
                        if len(param_dic_list) == 1:
                            headers = {"Content-type": "text/plain"}
                            response = make_response(records_to_text(data), headers)
                            return response
                        else:
                            archive.writestr(f"{cid}.csv", records_to_text(data))
                    else:
                        if len(param_dic_list) == 1:
                            headers = {"Content-type": "image/png"}
                            response = make_response(img.getvalue(), headers)
                            return response
                        else:
                            archive.writestr(f"{cid}.png", img.getvalue())
                except Exception as ex:
                    freqs = None
                    logging.debug(str(ex))
                    archive.writestr(f"{cid}.csv", "ERROR: " + msg)
                    continue

        if inventory is None and len(param_dic_list) == 1:
            return nodata_error(params)

        if freqs is None and len(param_dic_list) == 1:
            return error_500(Error.UNSPECIFIED)

        tmp.seek(0)
        headers = {"Content-Disposition": "attachment; filename=evalresp-resif.zip"}
        response = make_response(tmp.read(), headers)
        response.headers["Content-type"] = "application/x-zip-compressed"
        response.mimetype = "application/x-zip-comp"
        return response
    except Exception as ex:
        logging.exception(str(ex))
    finally:
        tmp.close()
        if response:
            nbytes = response.headers.get("Content-Length")
            logging.info(f"{nbytes} bytes rendered in {tictac(tic)} seconds.")
