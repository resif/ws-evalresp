# Webservice evalresp

## Play around with docker

```
docker build -t ws-evalresp ./
docker run --rm -e RUNMODE=test -p 8000:8000 --name ws-evalresp ws-evalresp:latest
```

Then :

```
wget -O - http://localhost:8000/1
```


## Run it in debug mode with flask

Create your virtual environment (eg. `python -m venv .venv && source .venv/bin/activate`)

Install the prereqs and run
```
pip install -r requirements.txt
FLASK_ENV=development FLASK_APP=start:app flask run
```

Then :
```
wget -O - http://localhost:5000/1
```


## RUNMODE builtin values

  * `production`
  * `test`
  * `local`
  * other values map to :

## Authors (dc@resif.fr)

- Jérôme Touvier
- Jonathan Schaeffer
- Philippe Bollard (Maintainer)
