README=$1
FILE=$2
TITLE=$3

pandoc -f markdown -t html -s "$README" -o "$FILE" --metadata title="$TITLE"

perl -i -0pe 's/__\*__/<strong>\*<\/strong>/s' $FILE
perl -i -0pe 's/loc=\–/loc=--/s' $FILE
perl -i -0pe 's/<header.*<\/header>/<p style="text-align:center;"> <img src=".\/static\/images\/logoresif.png" alt="logoresif" width="341" height="95"><\/p>/s' $FILE

if [[ $FILE == *doc.html ]] ; then

perl -i -0pe 's/<\/head>/<link rel="stylesheet" href=".\/static\/resifws.css"\/>
<a href=".\/local=en"> <img src=".\/static\/images\/en.png" alt="Français - Anglais"> <\/a>\n<\/head>/gms' $FILE

elif [[ $FILE == *doc_en.html ]] ; then

perl -i -0pe 's/<\/head>/<link rel="stylesheet" href=".\/static\/resifws.css"\/>
<a href=".\/local=fr"> <img src=".\/static\/images\/fr.png" alt="Anglais - Français"> <\/a>\n<\/head>/gms' $FILE

fi

mv $FILE ../templates/

# perl -i -0pe
# -i in-place
# -0 read entire file in one shot (without the multiline searches failed).
# -p run Perl code
# -e Perl will not look for a script filename in the argument list.
