# Webservice evalresp

This service provides access to the sensor instrumental responses of the RESIF seismic network.

## Query usage

    /query? (channel-options) [time-option] [frequency-options] [units-options] [plots-options] (output-options) [nodata=404]

    where:

    channel-options      ::  (net=<network>) (sta=<station>) (loc=<location>) (cha=<channel>)
    time-option          ::  (time=<date>)
    frequency-options    ::  [minfreq=<freq-Hz>] [maxfreq=<freq-Hz>] [nfreq=<number-of-frequencies>] [spacing=<lin|log>]
    units-options        ::  [units=<def|dis|vel|acc>]
    plot-options         ::  [width=<pixels>] [height=<pixels>] [annotate=<true|false>]
    output-options       ::  (format=<fap|cs|plot|plot-amp|plot-phase>)

    (..) required
    [..] optional

## Sample queries

<a href="http://ws.resif.fr/resifws/evalresp/1/query?net=FR&sta=CIEL&loc=00&cha=HHE&time=2017-01-01T00:00:00&format=plot">http://ws.resif.fr/resifws/evalresp/1/query?net=FR&sta=CIEL&loc=00&cha=HHE&time=2017-01-01T00:00:00&format=plot</a>

## Detailed descriptions of each query parameter

| Parameter  | Example  | Discussion                                                                       |
| :--------- | :------- | :--------------------------------------------------------------------------------|
| net[work]  | FR       | Seismic network name.                                                            |
| sta[tion]  | CIEL     | Station name.                                                                    |
| loc[ation] | 00       | Location code. Use loc=-- for empty location codes.                              |
| cha[nnel]  | HHZ      | Channel Code.                                                                    |
| time       | 2017-01-01T00:00:00 | Evaluate the response at the given time. If not specified, the current time is used. |
| minfreq    | 0.001   | The minimum frequency (Hz) at which response will be evaluated.                 |
| maxfreq    | 100     | The maximum frequency (Hz) at which response will be evaluated. Defaults to the channel sample rate or the frequency of sensitivity, whichever is larger. |
| nfreq      | 500     | Number frequencies at which response will be evaluated. Must be a positive integer no greater than 10000. |
| units      | acc     | Output unit. Must be: __def__ (unit in response metadata), __dis__ (displacement), __vel__ (velocity), __acc__ (acceleration). |
| spacing    | linear  | Scale frequency. Must be: __lin(ear)__ or __log(arithmic)__.                       |
| width      | 500     | The width of the generated plot. Defaults to 800.                               |
| height     | 400     | The height of the generated plot. Defaults to 600.                              |
| annotate   | false   | Displays the sensitivity, the frequency and sensitivity frequency.              |
| degrees    | false   | Displays the phase in degrees or in radians.                                    |
| format     | plot    | Output format. Must be: __fap__ (three column ASCII: frequency, amplitude, phase), __cs__ (three column ASCII: frequency, real, imaginary), __plot__ (amplitude and phase plot), __plot-amp__ (amplitude only plot), __plot-phase__ (phase only plot). |
| nodata     | 404     | Specify which HTTP status code is returned when no data is found (204 or 404) | 204    |

## Date and time formats

    YYYY-MM-DDThh:mm:ss[.ssssss] ex. 1997-01-31T12:04:32.123
    YYYY-MM-DD ex. 1997-01-31 (a time of 00:00:00 is assumed)

    where:

    YYYY    :: four-digit year
    MM      :: two-digit month (01=January, etc.)
    DD      :: two-digit day (01 through 31)
    T       :: date-time separator
    hh      :: two-digit hour (00 through 23)
    mm      :: two-digit number of minutes (00 through 59)
    ss      :: two-digit number of seconds (00 through 59)
    ssssss  :: one to six-digit number of microseconds (0 through 999999)

