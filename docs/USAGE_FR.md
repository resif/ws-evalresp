# Webservice evalresp

Ce service donne accès aux réponses instrumentales des capteurs du réseau sismique RESIF.

## Utilisation de la requête

    /query? (channel-options) [time-option] [frequency-options] [units-options] [plots-options] (output-options) [nodata=404]

    où :

    channel-options      ::  (net=<network>) (sta=<station>) (loc=<location>) (cha=<channel>)
    time-option          ::  (time=<date>)
    frequency-options    ::  [minfreq=<freq-Hz>] [maxfreq=<freq-Hz>] [nfreq=<number-of-frequencies>] [spacing=<lin|log>]
    units-options        ::  [units=<def|dis|vel|acc>]
    plot-options         ::  [width=<pixels>] [height=<pixels>] [annotate=<true|false>]
    output-options       ::  (format=<fap|cs|plot|plot-amp|plot-phase>)

    (..) requis
    [..] optionnel

## Exemples de requêtes

<a href="http://ws.resif.fr/resifws/evalresp/1/query?net=FR&sta=CIEL&loc=00&cha=HHE&time=2017-01-01T00:00:00&format=plot">http://ws.resif.fr/resifws/evalresp/1/query?net=FR&sta=CIEL&loc=00&cha=HHE&time=2017-01-01T00:00:00&format=plot</a>

## Descriptions détaillées de chaque paramètre de la requête

| Paramètre  | Exemple | Discussion                                                                    |
| :--------- | :------ | :---------------------------------------------------------------------------- |
| net[work]  | FR      | Nom du réseau sismique.                                                       |
| sta[tion]  | CIEL    | Nom de la station.                                                            |
| loc[ation] | 00      | Code de localisation. Utilisez loc=-- pour les codes de localisations vides.  |
| cha[nnel]  | HHZ     | Code de canal.                                                                |
| time       | 2017-01-01T00:00:00 | La réponse est évaluée à l'instant donné. Sans précision, le temps actuel est utilisé. |
| minfreq    | 0.001   | Fréquence minimum à laquelle la réponse sera évaluée. Par défaut à 0.00001 Hz.  |
| maxfreq    | 100     | Fréquence maximum à laquelle la réponse sera évaluée. Par défaut la valeur la plus grande entre la fréquence d'échantillonnage et la fréquence de sensibilité. |
| nfreq      | 500     | Nombre de fréquences pour lesquelles la réponse sera évaluée (10000 au maximum). |
| units      | acc     | Unité de sortie à choisir parmi __def__ (unité indiquée dans la réponse), __dis__ (déplacement), __vel__ (vitesse) et __acc__ (accélération). |
| spacing    | linear  | Échelle des fréquences linéaire (__lin__ ou __linear__) ou logarithmique à intervalles réguliers (__log__ ou __logarithmic__). |
| width      | 500     | Largeur de l'image de sortie (pixels). De 400 à 3000. Par défaut à 800.         |
| height     | 400     | Hauteur de l'image de sortie (pixels). De 400 à 3000. Par défaut à 600.         |
| annotate   | false   | Affiche la sensibilité, la fréquence de nyquist et la fréquence de sensibilité. |
| degrees    | false   | Affiche la phase en degrés ou en radians.                                       |
| format     | plot    | Format de sortie du fichier : __fap__ (trois colonnes au format texte : fréquence, amplitude, phase), __cs__ (trois colonnes au format texte : fréquence, partie réelle, partie imaginaire), __plot__ (amplitude et phase du diagramme de bode), __plot-amp__ (amplitude seule du diagramme de bode), __plot-phase__ (phase seule du diagramme de bode). |

## Formats des dates et des heures

    YYYY-MM-DDThh:mm:ss[.ssssss] ex. 1997-01-31T12:04:32.123
    YYYY-MM-DD ex. 1997-01-31 (une heure de 00:00:00 est supposée)

    avec :

    YYYY    :: quatre chiffres de l'année
    MM      :: deux chiffres du mois (01=Janvier, etc.)
    DD      :: deux chiffres du jour du mois (01 à 31)
    T       :: séparateur date-heure
    hh      :: deux chiffres de l'heure (00 à 23)
    mm      :: deux chiffres des minutes (00 à 59)
    ss      :: deux chiffres des secondes (00 à 59)
    ssssss  :: un à six chiffres des microsecondes en base décimale (0 à 999999)

